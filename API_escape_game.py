from flask import Flask, request, jsonify
from flask_mysqldb import MySQL

app = Flask(__name__)

# Configuration de la base de données
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'workshop'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'
app.config['MYSQL_PORT'] = 3308
  # Renvoie les résultats sous forme de dictionnaires

# Initialisation de l'extension MySQL
mysql = MySQL(app)

@app.route('/')
def index():
    return 'Server Works!'

# Ajout d'un groupe
@app.route('/ajouter_groupe/', methods=['POST'])
def ajouter_groupe():
    try:
        data = request.json  # Supposons que vous envoyez des données au format JSON

        # Insérer des données dans la table "equipe"
        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO equipe (nom_equipe) VALUES (%s)", (data['nom_equipe'],))
        mysql.connection.commit()
        cur.close()

        # Répondre avec un message de succès
        response = {
            'message': 'Groupe ajouté avec succès dans la base de données.'
        }
        return jsonify(response)

    except Exception as e:
        return str(e)

# Ajout d'un utilisateur
@app.route('/ajouter_utilisateur/', methods=['POST'])
def ajouter_utilisateur():
    try:
        data = request.json  # Supposons que vous envoyez des données au format JSON
        
        # recuperer id du groupe dans la table "groupe"
        cur = mysql.connection.cursor()
        cur.execute("SELECT id_equipe FROM equipe WHERE nom_equipe = %s", (data['nom_equipe'],))
        result = cur.fetchone()
        cur.close()

        if result:
            # Le groupe a été trouvé, renvoyer l'ID du groupe
             result['id_equipe']
        else:
            # Le groupe n'a pas été trouvé
            return jsonify({'message': 'Groupe non trouvé.'}), 404
        #  
        # Insérer des données dans la table "utilisateur"
        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO utilisateur (id_equipe, id_role, nom_user) VALUES (%s, %s, %s)",
                    (result['id_equipe'], data['id_role'], data['nom_user']))
        mysql.connection.commit()
        cur.close()

        # Répondre avec un message de succès
        response = {
            'message': 'Utilisateur ajouté avec succès dans la base de données.'
        }
        return jsonify(response)
 
    except Exception as e:
        return str(e)

# Ajout d'un classement
@app.route('/ajouter_classement/', methods=['POST'])
def ajouter_classement():
    try:
        data = request.json  # Supposons que vous envoyez des données au format JSON

        # Insérer des données dans la table "classement"
        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO classement (id_timer, id_utilisateur) VALUES (%s, %s)",
                    (data['id_timer'], data['id_utilisateur']))
        mysql.connection.commit()
        cur.close()

        # Répondre avec un message de succès
        response = {
            'message': 'Classement ajouté avec succès dans la base de données.'
        }
        return jsonify(response)

    except Exception as e:
        return str(e)
# recuperer les questions
@app.route('/recuperer_question/<int:id_question>', methods=['GET'])
def recuperer_question(id_question):
    try:
        cur = mysql.connection.cursor()
        cur.execute("SELECT * FROM question WHERE id_question = %s", (id_question,))
        question = cur.fetchone()
        cur.close()

        if question:
            # La question a été trouvée, renvoyer la question en tant que réponse JSON
            return jsonify(question)
        else:
            # La question n'a pas été trouvée
            return jsonify({'message': 'Question non trouvée.'}), 404

    except Exception as e:
        return str(e)
    
    # Récupérer un indice par son ID
@app.route('/recuperer_indice/<int:id_indice>', methods=['GET'])
def recuperer_indice(id_indice):
    try:
        cur = mysql.connection.cursor()
        cur.execute("SELECT * FROM indice WHERE id_indice = %s", (id_indice,))
        indice = cur.fetchone()
        cur.close()

        if indice:
            # L'indice a été trouvé, renvoyer l'indice en tant que réponse JSON
            return jsonify(indice)
        else:
            # L'indice n'a pas été trouvé
            return jsonify({'message': 'Indice non trouvé.'}), 404

    except Exception as e:
        return str(e) 

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5400, debug=True)

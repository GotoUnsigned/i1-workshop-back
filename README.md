"API de Gestion de Workshop" ou "API de Jeu en Équipe".

Cette API Flask est conçue pour gérer un système de jeu en équipe (workshop) en utilisant une base de données MySQL. Elle offre les fonctionnalités suivantes :

1. **Ajout de Groupe :** Vous pouvez ajouter un groupe en envoyant des données JSON via la route `/ajouter_groupe/`. L'ID du groupe est récupéré à partir du nom du groupe dans la table "equipe" de la base de données.

2. **Ajout d'Utilisateur :** Vous pouvez ajouter un utilisateur à un groupe en envoyant des données JSON via la route `/ajouter_utilisateur/`. L'ID du groupe est récupéré en fonction du nom du groupe dans la table "equipe", et l'utilisateur est associé à un rôle et à un nom.

3. **Ajout de Classement :** Vous pouvez ajouter un classement en envoyant des données JSON via la route `/ajouter_classement/`. Le classement est associé à un chronomètre (timer) et à un utilisateur.

4. **Récupération de Question :** Vous pouvez récupérer une question à partir de son ID en utilisant la route `/recuperer_question/<id_question>`. La question est renvoyée au format JSON.

5. **Récupération d'Indice :** Vous pouvez récupérer un indice à partir de son ID en utilisant la route `/recuperer_indice/<id_indice>`. L'indice est renvoyé au format JSON.

L'API est configurée pour se connecter à une base de données MySQL locale (sur `localhost`) et renvoie les résultats des requêtes SQL sous forme de dictionnaires. Elle écoute sur le port 5400 en mode de débogage pour faciliter le développement et les tests.

Cette API peut être utilisée pour gérer des données liées à un jeu en équipe, tel qu'un jeu de quiz, un jeu de chasse au trésor, ou d'autres jeux collaboratifs où des groupes d'utilisateurs interagissent, répondent à des questions, et collectent des indices. Elle offre une interface pour ajouter des groupes, des utilisateurs, des classements, et pour récupérer des questions et des indices à partir de leur ID respectif.
